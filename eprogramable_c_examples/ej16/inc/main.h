
#ifndef _MAIN_H
#define _MAIN_H
/*==================[inclusions]=============================================*/

#include "stdint.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*==================[macros and definitions]=================================*/
int main(void);

#define masc1 0xF


/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /*  */

