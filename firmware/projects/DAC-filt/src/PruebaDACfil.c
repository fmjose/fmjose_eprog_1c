/*
 * PruebaDAC.c
 *
 *  Created on: 26 may. 2020
 *      Author: Fran
 */


#include "../../DAC-filt/inc/PruebaDACfil.h"
#include "switch.h"
#include "led.h"
#include "delay.h"
#include "systemclock.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"

#define ttimer 2	/*Periodo de muestreo */
#define escala 4	/*Escala dato senial*/
#define umbral 150	/*Umbral deteccion picos*/

void GetAnalog(void);
void ConvertAD(void);
void ConvertDA(void);
void PrenderFiltro();
void SubirFC(void);
void ApagarFiltro(void);
void BajarFC(void);


uint8_t TABLA[] = { /*Senial ECG*/
		17,17,17,17,17,17,17,17,17,17,17,18,18,18,17,17,17,17,17,17,17,18,18,18,18,18,18,18,17,17,16,16,16,16,17,17,18,18,18,17,17,17,17,
		18,18,19,21,22,24,25,26,27,28,29,31,32,33,34,34,35,37,38,37,34,29,24,19,15,14,15,16,17,17,17,16,15,14,13,13,13,13,13,13,13,12,12,
		10,6,2,3,15,43,88,145,199,237,252,242,211,167,117,70,35,16,14,22,32,38,37,32,27,24,24,26,27,28,28,27,28,28,30,31,31,31,32,33,34,36,
		38,39,40,41,42,43,45,47,49,51,53,55,57,60,62,65,68,71,75,79,83,87,92,97,101,106,111,116,121,125,129,133,136,138,139,140,140,139,137,
		133,129,123,117,109,101,92,84,77,70,64,58,52,47,42,39,36,34,31,30,28,27,26,25,25,25,25,25,25,25,25,24,24,24,24,25,25,25,25,25,25,25,
		24,24,24,24,24,24,24,24,23,23,22,22,21,21,21,20,20,20,20,20,19,19,18,18,18,19,19,19,19,18,17,17,18,18,18,18,18,18,18,18,17,17,17,17,
		17,17,17};

const float tmuestreo=0.002;	/*Periodo de muestreo conversor AD*/
uint16_t dato,aux,datofil,cont=0,fc=10;	/*Variables para entrada, salida y filtro*/
bool prendido;			/*Bandera encendido filtro*/
float alfa;

timer_config my_timer = {TIMER_A,ttimer,&ConvertAD};
timer_config my_timer2 = {TIMER_B,4,&ConvertDA};
analog_input_config entA={CH1,AINPUTS_SINGLE_READ,&GetAnalog};
serial_config uarsito={SERIAL_PORT_PC,115200,NULL};



void SInit(void){
		SystemClockInit();						/*Inicializacion de timers, y puertos*/
		TimerInit(&my_timer);
		TimerInit(&my_timer2);
		TimerStart(TIMER_A);
		TimerStart(TIMER_B);
		SwitchesInit();
		SwitchActivInt(SWITCH_1,&PrenderFiltro);
		SwitchActivInt(SWITCH_2,&ApagarFiltro);
		SwitchActivInt(SWITCH_3,&SubirFC);
		SwitchActivInt(SWITCH_4,&BajarFC);
		LedsInit();
		AnalogOutputInit();
		AnalogInputInit(&entA);
		UartInit(&uarsito);


}

void ConvertAD(void){

	AnalogStartConvertion();	/*Funcio de interrupcion timer A*/

}
void ConvertDA(void){
													/*Funcion de interrupcion timer B*/
	if(cont<sizeof(TABLA)){							/*Logica recorrido de señal y salida por DAC*/
	AnalogOutputWrite(TABLA[cont]*escala);
	cont++;}
	else{
		cont=0;
	}

}
void GetAnalog(void){
	AnalogInputRead(CH1,&dato); 	/*Lectura de dato entradaa analogica*/
	if(cont>0){
	alfa=tmuestreo/((1/(2*3.14*fc))+tmuestreo);			/*calculo coeficiente filtro*/
	datofil=aux+alfa*(dato-aux);						/*filtro dato*/
	}
	else{datofil=dato;}
	if(dato>=umbral){LedOn(LED_1);}					/*Deteccion de picos e indiciacion mediante LED de QRS*/
	if(dato<umbral){LedOff(LED_1);}
	UartSendString(SERIAL_PORT_PC,UartItoa(dato,10));	/*SAlida de datos mediante UART*/
	UartSendString(SERIAL_PORT_PC,",");
	if(prendido){
	UartSendString(SERIAL_PORT_PC,UartItoa(datofil,10));}
	UartSendString(SERIAL_PORT_PC," \r");
	aux=datofil;


}

void PrenderFiltro(){
	prendido=1;					/*Funcion de interrupcion para encendido de filtros*/
	LedOn(LED_2);

}
void ApagarFiltro(){
	prendido=0;
	LedOff(LED_2);			/*Funcion de interrupcion para apagado de filtro*/

}
void SubirFC(){
	fc++;

}
void BajarFC(){
	fc--;
}

int main(){

	SInit();
	while(1){}

	}




