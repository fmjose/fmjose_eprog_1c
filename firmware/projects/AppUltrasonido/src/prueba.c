/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../AppUltrasonido/inc/prueba.h"       /* <= own header */

#include "hc_sr4.h"
#include "switch.h"
#include "led.h"
#include "delay.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"

void Medir(void);
void Entrada_Uart(void);
bool check=0,check2=1,checkT=0;
uint8_t datoent;
timer_config my_timer = {TIMER_A,1000,&Medir};
serial_config uarsito={SERIAL_PORT_PC,115200,&Entrada_Uart};

void IndicarDistancia(uint16_t dist)
{
						if(dist<=10)
						{
						LedsOffAll();
						LedOn(LED_RGB_B);
						//DelaySec(2);
						//check2=0;
						}

						if((dist>10)&&(dist<=20))
						{
						LedsOffAll();
						LedOn(LED_RGB_B);
						LedOn(LED_1);
						//DelaySec(2);
						//check2=0;
						}

						if(dist>20&&dist<=30)
						{
						LedsOffAll();
						LedOn(LED_1);
						LedOn(LED_2);
						LedOn(LED_RGB_B);
						//DelaySec(2);
						//check2=0;
						}

						if(dist>30)
						{
						LedsOffAll();
						LedOn(LED_1);
						LedOn(LED_2);
						LedOn(LED_RGB_B);
						LedOn(LED_3);
						//DelaySec(2);
						//check2=0;
						}
}

void S1(){
	check=!check;
	DelayMs(20);
	}
void S2(){
	check2=!check2;
	DelayMs(20);
}
void Entrada_Uart(void){
	UartReadByte(SERIAL_PORT_PC,&datoent);
	if(datoent=='O'){check=!check;}
	if(datoent=='H'){check2=!check2;}
}

void Medir(void){
	checkT=1;
}
void SInit(){
		SystemClockInit();
		TimerInit(&my_timer);
		TimerStart(TIMER_A);
		HcSr04Init(T_FIL2,T_FIL3);
		SwitchesInit();
		Cal1();
		LedOn(LED_RGB_B);
		LedOn(LED_RGB_G);
		LedOn(LED_RGB_R);
		DelaySec(3);
		LedOff(LED_RGB_B);
		LedOff(LED_RGB_G);
		LedOff(LED_RGB_R);
		SwitchActivInt(SWITCH_2,S2);
		SwitchActivInt(SWITCH_1,S1);
		UartInit(&uarsito);

}


int main(void)
{
	//while(1){
		SInit();
		uint16_t d=0;

		//uint8_t s=1;



		while(1)
		{
			if(check)
			{
				if(checkT){

				checkT=0;

				if(check2)
				{
				d=HcSr04ReadDistanceCentimeters();
				IndicarDistancia(d);

				}
				UartSendString(SERIAL_PORT_PC,"Distancia: ");
				UartSendString(SERIAL_PORT_PC,UartItoa(d,10));
				UartSendString(SERIAL_PORT_PC," \r\n");

				}

			}
			else
			{
				LedsOffAll();
			}


		}




}




/*==================[end of file]============================================*/

