/*
 * PruebaDAC.c
 *
 *  Created on: 26 may. 2020
 *      Author: Fran
 */


#include "../../ParcialEj2/inc/Parcial.h"
#include "systemclock.h"
#include "timer.h"
#include "analog_io.h"
#include "uart.h"

#define ttimer 25	/*Periodo de muestreo Conversor AD*/
#define tsend 1000 /*Periodo de envio datos UART*/
void GetAnalog(void);
void ConvertAD(void);
void Salida(void);


uint16_t dato,sal,aux,cont=0;


timer_config my_timer = {TIMER_A,ttimer,&ConvertAD};
timer_config my_timer2 = {TIMER_B,tsend,&Salida};
analog_input_config entA={CH1,AINPUTS_SINGLE_READ,&GetAnalog};
serial_config uarsito={SERIAL_PORT_PC,115200,NULL};



void SInit(void)
{
	SystemClockInit();						/*Inicializacion de timers, y puertos*/
	TimerInit(&my_timer);
	TimerInit(&my_timer2);
	TimerStart(TIMER_A);
	TimerStart(TIMER_B);
	AnalogOutputInit();
	AnalogInputInit(&entA);
	UartInit(&uarsito);


}

void ConvertAD(void)
{

	AnalogStartConvertion();	/*Funcio de interrupcion timer A*/

}

void Salida(void){
													/*Funcion de interrupcion timer B*/
	UartSendString(SERIAL_PORT_PC,UartItoa(aux,10));	/*SAlida de datos mediante UART*/
	UartSendString(SERIAL_PORT_PC,"\n\r");
}

void GetAnalog(void)
{
	AnalogInputRead(CH1,&dato); 	/*Lectura de dato entradaa analogica*/
	if(cont<(tsend/ttimer))
	{
		aux=aux+dato;
			/*Promediacion cada 1 seg*/
		cont++;
	}
	if(cont==39)
	{
		sal=aux/(cont+1);
		cont=0;
		aux=0;
	}
}



int main()
{

	SInit();
	while(1){}

}




