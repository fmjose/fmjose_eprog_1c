/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../../ParcialEj1/inc/ParcialEj1.h"       /* <= own header */

#include "hc_sr4.h"
#include "delay.h"
#include "systemclock.h"
#include "timer.h"
#include "uart.h"

#define ttimer 20 /*Periodo de envio*/
#define ttimer2 18 /*Periodo de muestreo*/
void Medir(void);
void Enviar(void);

uint16_t d=0,pir;

timer_config my_timer = {TIMER_A,ttimer2,&Medir};
timer_config my_timer2 = {TIMER_B,ttimer,&Enviar};
serial_config uarsito={SERIAL_PORT_PC,115200,NULL};

void Medir(void)
{/*Funcion de servicio a la interrupcion del timer A,
mide la distancia*/
	d=HcSr04ReadDistanceCentimeters();

}
void Enviar(void)
{
	/*Funcion de servicio al timer B, envia el dato por UART*/

	UartSendString(SERIAL_PORT_PC,UartItoa(d*pir,10));
	UartSendString(SERIAL_PORT_PC,"cm3\r\n");

}


void SInit()
{		/*Inicializacion timers,configuracion sensor y configuracion UART*/
	SystemClockInit();
	TimerInit(&my_timer);
	TimerInit(&my_timer2);
	TimerStart(TIMER_B);
	TimerStart(TIMER_A);
	HcSr04Init(T_FIL2,T_FIL3);
	UartInit(&uarsito);
	pir=6*6*3.14;

}


int main(void)
{
	SInit();
	while(1)
	{
	}


}




/*==================[end of file]============================================*/

