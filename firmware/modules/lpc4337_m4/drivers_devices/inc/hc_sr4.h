/*
 * hc_sr4.h
 *
 *  Created on: 22 abr. 2020
 *      Author: Fran
 */

#ifndef _HC_SR4_H
#define _HC_SR4_H
#include "gpio.h"
#include "bool.h"

#ifdef __cplusplus
extern "C" {
#endif


/*==================[cplusplus]==============================================*/


/*==================[external functions declaration]=========================*/

/*==================[end of file]============================================*/


/*==================[inclusions]=============================================*/



/*==================[cplusplus]==============================================*/

/*#ifdef __cplusplus
extern "C" {
#endif


#ifdef __cplusplus
}
#endif*/

/*==================[external functions declaration]=========================*/


/*Inicializacion del dispositivo, asignacion de puertos*/
bool HcSr04Init(gpio_t echo, gpio_t trigger);


/*Funcion que devuelve el valor de la distancia en cm*/
int16_t HcSr04ReadDistanceCentimeters(void);


/*Funcion que devuelve la distancia en pulgadas */
int16_t HcSr04ReadDistanceInches(void);


/*De inicializador*/
bool HcSr04Deinit(gpio_t echo, gpio_t trigger);


/*Funcion de calibracion del dispositivo
 * se debe disponer el objeto a 2cm */
void Cal1(void);

/*Funcion que realiza la calibracion a la distancia final
 * se debe colocar el objeto a 120 cm
 */

void Cal2(void);



/*==================[end of file]============================================*/


#ifdef __cplusplus
}
#endif


#endif /* MODULES_LPC4337_M4_DRIVERS_DEVICES_INC_HC_SR4_H_ */
