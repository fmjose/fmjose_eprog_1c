/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * FMJose -
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"       /* <= own header */
#include "delay.h"
#include "led.h"
#include "systemclock.h"

/*==================[macros and definitions]=================================*/


/*==================[internal data definition]===============================*/
int32_t N=35;
gpio_t Echo; gpio_t Trigger;
/*==================[internal functions declaration]=========================*/

/*Funcion que obtiene el tiempo de viaje del pulso*/
uint16_t GetT()
{
	uint16_t count=0, check1=1;
	GPIOOn(Trigger);
	DelayUs(10);
	GPIOOff(Trigger);
	while(!GPIORead(Echo))
	{}
	while(check1)
	{
		while(GPIORead(Echo))
		{
			DelayUs(1);
			count++;
		}
	check1=0;
	}
	return count;
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
bool HcSr04Init(gpio_t echo, gpio_t trigger){
	Echo=echo;
	Trigger=trigger;
	GPIOInit(echo, GPIO_INPUT);
	GPIOInit(trigger, GPIO_OUTPUT);
	SystemClockInit();

	LedsInit();
	LedOn(LED_3);
	DelaySec(1);
	LedOff(LED_3);

	return 1;
}

int16_t HcSr04ReadDistanceCentimeters(void)
{
	int16_t a=GetT()/N;
	return a;
}

int16_t HcSr04ReadDistanceInches(void)
{
	int16_t a=GetT()/(N+90);
	return a;
}

bool HcSr04Deinit(gpio_t echo, gpio_t trigger){

	GPIODeinit();
	return 1;
}

void Cal1(void)
{
	int16_t check=GetT()/N;
	int8_t cont=0;

		while((check-2)>2)
		{
			if((check-2)>1)
			{
				N+=1;
				check=GetT()/N;
				cont++;
			}
			if((check-2)<-1)
			{
				N-=1;
				check=GetT()/N;
				cont++;
			}
			if(cont==5){
				break;
			}
		}

		LedOn(LED_1);
		DelaySec(1);
		LedOff(LED_1);
}

void Cal2(void)
{
	int16_t check=GetT()/N;
		while((check-120)>0.05)
		{
			if((check-120)>0)
			{
				N+=1;
			}
			if((check-120)<0)
			{
				N-=1;
			}
		}
		for(uint8_t i=0;i<4;i++)
		{
		LedOn(LED_3);
		DelaySec(1);
		LedOff(LED_3);
		}
}



/*==================[end of file]============================================*/

