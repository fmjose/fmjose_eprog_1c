/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/
#include "main.h"
#include "stdint.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*==================[internal functions declaration]=========================*/
typedef struct{
	uint8_t port;	/*Definicion struct config*/
	uint8_t pin;
	uint8_t dir;

}gpioConf_t;

void Control_Puerto(uint8_t bcd, gpioConf_t *hard){

	if((bcd&m0)==m0){	/*compara con 0001*/
		hard[0].dir=1;
		printf("El puerto %d.%d esta en alto \n \r",hard[0].port,hard[0].pin);

	}
	if((bcd&m1)==m1){ /*compara con 0010*/
		hard[1].dir=1;
				printf("El puerto %d.%d esta en alto \n \r ",hard[1].port,hard[1].pin);

		}
	if((bcd&m2)==m2){	/*compara con 0100*/
		hard[1].dir=1;
				printf("El puerto %d.%d esta en alto \n \r",hard[2].port,hard[2].pin);

		}
	if((bcd&m3)==m3){	/*compara con 1000*/
		hard[1].dir=1;
				printf("El puerto %d.%d esta en alto \n \r",hard[3].port,hard[3].pin);

		}

	if((bcd&m0)==0&&(bcd&m0)==0&&(bcd&m0)==0&&(bcd&m0)==0){
			printf("Todos los puertos estan en bajo");
		}
		}






int main(void)
{	uint8_t num=5;
	gpioConf_t puertos[4], *pt;
	pt=&puertos[0];
	puertos[0].port=1;puertos[0].pin=4;
	puertos[1].port=1;puertos[1].pin=5;
	puertos[2].port=1;puertos[2].pin=6;
	puertos[3].port=2;puertos[3].pin=14;

	Control_Puerto(num,pt);

  return 0;
}

/*==================[end of file]============================================*/

