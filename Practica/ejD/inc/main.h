
#ifndef _MAIN_H
#define _MAIN_H
/*==================[inclusions]=============================================*/
#define m0 (1<<0)	/*mascaras para cada bit*/
#define m1 (1<<1)
#define m2 (1<<2)
#define m3 (1<<3)

/*==================[macros and definitions]=================================*/
int main(void);



/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/


#endif /*  */

