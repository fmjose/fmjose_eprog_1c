/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "main.h"
#include "stdint.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*==================[macros and definitions]=================================*/
typedef struct{
	uint8_t n_led;
	uint8_t n_ciclos;
	uint8_t periodo;
	uint8_t mode; /*OFF=0-ON=1-TOGGLE=3*/
}LED;

/*==================[internal functions declaration]=========================*/

void moverled(LED *led){
	uint8_t n,t=0;
	switch (led->mode){
	{case(1):{
		switch(led->n_led){

		case(1):{
			printf("Enciende led 1");
		}
		break;
		case(2):{
					printf("Enciende led 2");
				}break;
		case(3):{
					printf("Enciende led 3");
				}break;
		case(4):{
					printf("Enciende led 4");
				}break;
		}
	}break;
	case(0):{
		switch(led->n_led){
		case(1):{
			printf("Apago led 1");
		}break;
						case(2):{
									printf("Apaga led 2");
								}break;
						case(3):{
									printf("Apaga led 3");
								}break;
						case(4):{
									printf("Apaga led 4");
								}break;
						}

			}break;
	case(3):{
		switch(led->n_led){
			case(1):{
				for(n=0;n<led->n_ciclos;n++){
					while(t<led->periodo){
								printf("Prendo led 1 ");
								t++;}
					printf("Espero");
				}
			}
				break;
		case(2):{
			for(n=0;n<led->periodo;n++){
				while(t<led->n_ciclos){
			printf("Parendo led 2 ");
			t++;}
				printf("Espero");
			}

						}break;
		case(3):{
			for(n=0;n<led->periodo;n++){
				while(t<led->n_ciclos){
							printf("Prendo led 3 ");
							t++;}
				printf("Espero");
							}
						}break;
		case(4):{
			for(n=0;n<led->n_ciclos;n++){
				while(t<led->periodo){
							printf("Prendo led 4 ");
							t++;}
				printf("Espero");		}

			}break;
				}

	}break;
		}
	}}

int main(void)
{
    LED ledsito, *led1;
    ledsito.mode=3;
    ledsito.n_led=1;
    ledsito.periodo=5;
    ledsito.n_ciclos=3;
    led1=&ledsito;

    moverled(led1);





	return 0;
}

/*==================[end of file]============================================*/

